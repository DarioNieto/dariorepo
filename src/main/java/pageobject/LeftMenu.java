package pageobject;

import infra.DriverFramework;
import org.openqa.selenium.By;

public class LeftMenu extends AbstractPage{
    private static final By onboardingLink  = By.xpath("//a[contains(@href,\"onboarding\")]");

    public LeftMenu(DriverFramework driverFramework){
        super(driverFramework, onboardingLink);
    }

    public OnboardingPage onboardingClick(){
        driverFramework.doClick(onboardingLink);
        return new OnboardingPage(driverFramework);
    }

}
