package pageobject;

import infra.ConfigObtain;
import infra.DriverFramework;
import org.openqa.selenium.By;

public class OnboardingPage extends AbstractPage{

    private static final By addInvestorButton = By.xpath("//button[contains(@class, \"btn btn-primary d-flex justify-content-center align-items-center cp-button-with-loader\")]");
    /* ----- General Information ----- */
    private static final By firstNameTextField = By.name("firstName");
    private static final By middleNameTextField = By.name("middleName");
    private static final By lastNameTextField = By.name("lastName");
    private static final By emailTextField = By.name("email");
    private static final By countryCombo = By.name("countryCode");
    private static final By stateCombo = By.name("state");
    private static final By okButtonGeneralInfo = By.xpath("//button[contains(text(), \"OK\")]");
    /* ----- Documents ----- */
    private static final By addDocumentButton = By.xpath("//button/span[text()=' Add document ']");
    private static final By categoryCombo = By.xpath("(//select[@class='custom-select form-group'])[1]");
    private static final By addDocumentInput = By.xpath("(//input[@type='file'])");
    private static final By addDocumentSuccess = By.xpath("'//div[@class='dz-preview dz-processing dz-image-preview dz-success dz-complete']");


    public OnboardingPage(DriverFramework driverFramework){
        super(driverFramework, addInvestorButton);
    }

    public void addInvestorClick(){
        driverFramework.doClick(addInvestorButton);
    }


    /* ----- General Information ----- */

    public void typeFirstName(String value){
        driverFramework.waitForElement(firstNameTextField);
        driverFramework.typeText(firstNameTextField, value);
    }

    public void typeMiddleName(String value){
        driverFramework.typeText(middleNameTextField, value);
    }

    public void typeLastName(String value){
        driverFramework.typeText(lastNameTextField, value);
    }

    public void typeEmail(String value){
        driverFramework.typeText(emailTextField, value);
    }

    public void pickCountry(String value){
        driverFramework.selectCombo(countryCombo, value);
    }

    public void pickState(String value){
        driverFramework.waitForElement(stateCombo);
        driverFramework.selectCombo(stateCombo, value);
    }

    public void clickOkGeneralInfo(){
        driverFramework.doClick(okButtonGeneralInfo);
    }


    /* ----- Documents ----- */

    public void clickAddDocument(){
        driverFramework.waitForElement(addDocumentButton);
        driverFramework.doClick(addDocumentButton);
    }

    public void addDocument(String path){
        //todo: For some reason this stop working and dunno how to fix it, I'll hardcode the path just to continue
        //driverFramework.typeText(addDocumentInput, ConfigObtain.getInstance().getPath(path));
        driverFramework.typeText(addDocumentInput, path);
        driverFramework.waitForElement(addDocumentSuccess);
    }

    public void pickCategory(String value){
        driverFramework.waitForElement(categoryCombo);
        driverFramework.selectCombo(categoryCombo, value);
    }

    public void addNewInvestor(String firstName, String middleName, String lastName, String email, String country, String state, String passportPath,
                               String category){
        addInvestorClick();
        typeFirstName(firstName);
        typeMiddleName(middleName);
        typeLastName(lastName);
        typeEmail(email);
        pickCountry(country);
        pickState(state);
        clickOkGeneralInfo();
        clickAddDocument();
        pickCategory(category);
        addDocument(passportPath);
    }
}
