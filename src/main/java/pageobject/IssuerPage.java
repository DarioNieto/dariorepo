package pageobject;

import infra.DriverFramework;
import org.openqa.selenium.By;

public class IssuerPage extends AbstractPage{

    private static final By issuerSearchTextField = By.xpath("//input[@class='form-control form-control-m']");
    private static final String issuerFoundLabelXpath = "(//h3[contains(text(), \"$ISSUER_NAME$\")])";
    private static final By viewIssuerButton = By.xpath("//button[contains(@class, \"btn btn-outline-primary btn-block\")]");


    public IssuerPage(DriverFramework driverFramework){
        super(driverFramework, issuerSearchTextField, viewIssuerButton);
    }

    public void searchForIssuer(String value){
        driverFramework.typeText(issuerSearchTextField, value);
    }

    public void waitForIssuerFound(String value){
        By issuerFoundLblModified = By.xpath(issuerFoundLabelXpath.replace("$ISSUER_NAME$", value));
        driverFramework.waitForElement(issuerFoundLblModified);
    }

    public void clickViewIssuer(){
        driverFramework.doClick(viewIssuerButton);
    }

    public LeftMenu pickIssuer(String issuer_name){
        searchForIssuer(issuer_name);
        waitForIssuerFound(issuer_name);
        clickViewIssuer();
        return new LeftMenu(driverFramework);
    }

}
