package pageobject;

import infra.DriverFramework;
import org.openqa.selenium.By;

public class LoginPage extends AbstractPage{

    private static final By emailTextField = By.name("email");
    private static final By passwordTextField = By.name("password");
    private static final By signInButton = By.xpath("//button[contains(@class,\"btn btn-outline-primary btn-block mb-3\")]");



    public LoginPage(DriverFramework driverFramework){
        super(driverFramework, emailTextField, passwordTextField);
    }

    public void setEmail(String value){
        driverFramework.typeText(emailTextField, value);
    }

    public void setPassword(String value) {
        driverFramework.typeText(passwordTextField, value);
    }

    public void clickSignIn(){
        driverFramework.doClick(signInButton);
    }

    public IssuerPage performLogin(String login_user, String login_password){
        setEmail(login_user);
        setPassword(login_password);
        clickSignIn();
        return new IssuerPage(driverFramework);
    }
}
