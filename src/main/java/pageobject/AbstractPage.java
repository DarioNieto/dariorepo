package pageobject;

import infra.DriverFramework;
import infra.Logger;
import org.openqa.selenium.By;

public abstract class AbstractPage {

    protected final DriverFramework driverFramework;

    public AbstractPage(DriverFramework paramDriverFramework, By... by_elements) {
        driverFramework = paramDriverFramework;
        waitForElements(by_elements);
    }

    public void waitForElements(By... by_elements){
        int elementCounter = 1;
        Logger.getInstance().toLog("-> Starting waiting for elements");
        for (By elem : by_elements){
            //Logging
            String elementCounterString = Integer.toString(elementCounter);
            String elementListSizeString = Integer.toString(by_elements.length);
            Logger.getInstance().toLog("Waiting for element: " + elementCounterString + "/" + elementListSizeString + " - " + elem);
            elementCounter++;

            //Waiting for element
            driverFramework.waitForElement(elem);
        }
        Logger.getInstance().toLog("- All elements were found");
    }
}
