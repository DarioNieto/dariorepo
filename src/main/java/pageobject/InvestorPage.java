package pageobject;

import infra.DriverFramework;
import org.openqa.selenium.By;

public class InvestorPage extends AbstractPage{

    private static final By firstNameLabel = By.xpath("//td[contains(text(), 'First name')]/following-sibling::td/strong");
    private static final By middleNameLabel = By.xpath("//td[contains(text(), \"Middle name\")]/following-sibling::td/strong");
    private static final By lastNameLabel = By.xpath("//td[contains(text(), \"Last name\")]/following-sibling::td/strong");
    private static final By emailLabel = By.xpath("//td[contains(text(), \"Email\")]/following-sibling::td/strong");
    private static final By countryLabel = By.xpath("//td[contains(text(), \"Country\")]/following-sibling::td/strong");
    private static final By stateLabel = By.xpath("//td[contains(text(), \"State\")]/following-sibling::td/strong");



    public InvestorPage(DriverFramework driverFramework){
        super(driverFramework, firstNameLabel, lastNameLabel);
    }

    public String getFirstName(){
        return driverFramework.getElementText(firstNameLabel);
    }

    public String getMiddleName(){
        return driverFramework.getElementText(middleNameLabel);
    }

    public String getLastName(){
        return driverFramework.getElementText(lastNameLabel);
    }

    public String getEmail(){
        return driverFramework.getElementText(emailLabel);
    }

    public String getCountry(){
        return driverFramework.getElementText(countryLabel);
    }

    public String getState(){
        return driverFramework.getElementText(stateLabel);
    }

}
