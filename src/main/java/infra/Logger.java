package infra;

public class Logger {

    private static Logger instance = null;

    //Open file or log location
    private Logger(){

    }

    public static Logger getInstance() {
        if(instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    //Implement how to log in here
    public void toLog(String msg){
        System.out.println(msg);
    }

}
