package infra;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverFramework {

    public final WebDriver driver;
    public final WebDriverWait wait;

    public DriverFramework() {
        driver = WebDriverFactory.generateDriver();
        wait = new WebDriverWait(driver, ConfigObtain.getIntConfig("intWaitTime"));
    }

    public void getUrl(String url){
        driver.get(url);
    }

    public void waitForElement(By element){
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public WebElement getElement(By element){
        return driver.findElement(element);
    }

    public void typeText(By element, String value){
        getElement(element).sendKeys(value);
    }

    public void doClick(By element){
        getElement(element).click();
    }

    public void selectCombo(By element, String value){
        Select select = new Select(getElement(element));
        select.selectByValue(value);
    }

    public String getElementText(By element){
        return getElement(element).getText();
    }

}
