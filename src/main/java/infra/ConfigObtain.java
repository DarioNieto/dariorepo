package infra;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.Random;

public class ConfigObtain {

    private static ConfigObtain instance = null;
    private final Properties prop = new Properties();

    private ConfigObtain() {
        //todo: For some reason this stop working and dunno how to fix it, I'll hardcode the path just to continue
        //Get the paths
        //String classicPath = getPath("../resources/configValues.properties");
        //String userPasswordPath = getPath("../resources/userPassword.properties");
        String classicPath = "C:/Users/argie/IdeaProjects/dariorepo/src/main/java/resources/configValues.properties";
        String userPasswordPath = "C:/Users/argie/IdeaProjects/dariorepo/src/main/java/resources/userPassword.properties";

        try {
            //Load the prop variable
            InputStream classicPathStream = new FileInputStream(classicPath);
            InputStream userPasswordStream = new FileInputStream(userPasswordPath);
            prop.load(classicPathStream);
            prop.load(userPasswordStream);
        } catch (Exception e) {
            System.out.println("There was a problem opening properties file");
            e.printStackTrace();
        }
    }

    public static ConfigObtain getInstance( ) {
        if(instance == null) {
            instance = new ConfigObtain();
        }
        return instance;
    }

    public String getPath(String fileLocation){
        URL url_path = ConfigObtain.class.getResource(fileLocation);
        return String.valueOf(url_path).replace("file:/", "");
    }

    private Properties getProperties() {
        return prop;
    }

    private static String generateString(int length){
        Random rng = new Random();
        String charLower = "abcdefghijklmnopqrstuvwxyz";
        String charUpper = charLower.toUpperCase();
        String dataForRandom = charLower + charUpper;
        char[] text = new char[length];
        for (int i = 0; i < length; i++){
            text[i] = dataForRandom.charAt(rng.nextInt(dataForRandom.length()));
        }
        return new String(text);
    }

    public static String getConfig(String value) {
        String obtainedValue = getInstance().getProperties().getProperty(value);
        return obtainedValue.replace("$RANDOM$", generateString(10));
    }

    public static int getIntConfig(String value){
        return Integer.parseInt(getConfig(value));
    }

}
