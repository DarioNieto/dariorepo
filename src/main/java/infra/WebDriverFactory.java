package infra;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.URL;

public class WebDriverFactory {

    public static WebDriver generateDriver(){
        String driverFileName = "main/java/infra/chromedriver.exe";

        //todo: For some reason this stop working and dunno how to fix it, I'll hardcode the path just to continue
        //URL url_path = WebDriverFactory.class.getResource(driverFileName);
        //String string_path = String.valueOf(url_path).replace("file:/", "");
        String string_path = "C:/Users/argie/IdeaProjects/dariorepo/src/main/java/infra/chromedriver.exe";

        System.setProperty("webdriver.chrome.driver", string_path);
        ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }
}
