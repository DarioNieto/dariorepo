package tests;

import infra.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageobject.*;
import infra.ConfigObtain;


public class TestFlow extends AbstractTest {

    @Test(description = "Adds an Investor and validates it")
    public void tc1(){

        /* Do login */
        LoginPage loginPage = new LoginPage(driverFramework);
        IssuerPage issuerPage = loginPage.performLogin(ConfigObtain.getConfig("strLoginUser"), ConfigObtain.getConfig("strLoginPassword"));

        /* Search for Issuer */
        LeftMenu leftMenu = issuerPage.pickIssuer(ConfigObtain.getConfig("strIssuerToPick"));

        /* Click in Onboarding */
        OnboardingPage onboardingPage = leftMenu.onboardingClick();

        /* Add new Investor */
        String firstName = ConfigObtain.getConfig("strInvestorName");
        String middleName = ConfigObtain.getConfig("strInvestorMiddleName");
        String lastName = ConfigObtain.getConfig("strInvestorLastName");
        String email = ConfigObtain.getConfig("strInvestorEmail");
        String country = ConfigObtain.getConfig("strCountryCode");
        String countryFullName = ConfigObtain.getConfig("strCountryFullName");
        String state = ConfigObtain.getConfig("strStateCode");
        String stateFullName = ConfigObtain.getConfig("strStateFullName");
        String passportPath = ConfigObtain.getConfig("strPassportPath");
        String category = ConfigObtain.getConfig("strCategory");
        onboardingPage.addNewInvestor(firstName, middleName, lastName, email, country, state, passportPath, category);

        /* Validate investor data */
        InvestorPage investorPage = new InvestorPage(driverFramework);
        softAssertion.assertEquals(investorPage.getFirstName(), firstName);
        softAssertion.assertEquals(investorPage.getMiddleName(), middleName);
        softAssertion.assertEquals(investorPage.getLastName(), lastName);
        softAssertion.assertEquals(investorPage.getEmail(), email);
        softAssertion.assertEquals(investorPage.getCountry(), countryFullName);
        softAssertion.assertEquals(investorPage.getState(), stateFullName);
    }

}
