package tests;

import infra.ConfigObtain;
import infra.DriverFramework;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

public class AbstractTest {

    protected DriverFramework driverFramework;
    protected SoftAssert softAssertion;

    @BeforeMethod
    public void setup(){

        driverFramework = new DriverFramework();
        softAssertion= new SoftAssert();

        /* Go to login page */
        String user = ConfigObtain.getConfig("strBasicAuthUser");
        String pwd = ConfigObtain.getConfig("strBasicAuthPwd");
        String url = ConfigObtain.getConfig("strLoginUrl");
        String completeUrl = "https://" + user + ":" + pwd + "@"+ url;
        driverFramework.getUrl(completeUrl);
        System.out.println("Before method executed");
    }

    @AfterMethod
    public void tearDown(){
        //driver.close();
        System.out.println("After method executed");
    }

}
